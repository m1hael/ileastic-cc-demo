CREATE TABLE thread_database FOR SYSTEM NAME threaddb (
  id INT GENERATED ALWAYS AS IDENTITY NOT NULL,
  tx INT NOT NULL,
  job CHAR(26) NOT NULL,
  thread CHAR(16) NOT NULL,
  ts TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
) RCDFMT threaddbf1;

LABEL ON TABLE thread_database
    is 'Demo Project : Thread Database';
 
LABEL ON COLUMN thread_database ( 
    id is 'Id', 
    tx is 'Transaction',
    job is 'Job', 
    thread is 'Thread',
    ts is 'Create Timestamp'
); 