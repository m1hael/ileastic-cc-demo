#
# Demo Project : ILEastic and Commitment Control
#

#-------------------------------------------------------------------------------
# User-defined part start
#

# BIN_LIB is the destination library for the program objects.
BIN_LIB=SHARED

#
# User-defined part end
#-------------------------------------------------------------------------------


all: env files compile bind

env:
	system -i "CRTJRNRCV JRNRCV($(BIN_LIB)/THREADDB) TEXT('Demo Project : Journal Receiver')"
	system -i "CRTJRN JRN($(BIN_LIB)/THREADDB) JRNRCV($(BIN_LIB)/THREADDB) MNGRCV(*SYSTEM) DLTRCV(*YES) TEXT('Demo Project : Journal')"

compile:
	$(MAKE) -C src/ compile $*

files:
	$(MAKE) -C src/ files $*
	-system -i "STRJRNPF FILE($(BIN_LIB)/THREADDB) JRN($(BIN_LIB)/THREADDB) IMAGES(*BOTH)"

bind:
	$(MAKE) -C src/ bind $*

clean:
	$(MAKE) -C src/ clean $*

purge:
	-system -i "ENDJRNPF FILE($(BIN_LIB)/THREADDB) JRN($(BIN_LIB)/THREADDB)"
	-system -i "DLTJRN JRN($(BIN_LIB)/THREADDB)"
	-system -i "DLTOBJ $(BIN_LIB)/THREADDB OBJTYPE(*FILE) RMVMSG(*YES)"
	$(MAKE) -C src/ purge $*

.PHONY:
